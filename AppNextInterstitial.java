/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.appnext.appnextsdk.Appnext;
import com.appnext.appnextsdk.NoAdsInterface;
import com.appnext.appnextsdk.OnAdLoadInterface;
import com.appnext.appnextsdk.PopupClickedInterface;
import com.appnext.appnextsdk.PopupClosedInterface;
import com.appnext.appnextsdk.PopupOpenedInterface;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.MoPubErrorCode;

public class AppNextInterstitial extends CustomEventTemplate implements OnAdLoadInterface, NoAdsInterface, PopupOpenedInterface, PopupClickedInterface, PopupClosedInterface {
    private CustomEventInterstitialListener mInterstitialListener;
    private Appnext mAppnext;

    @Override
    protected void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener,
            Map<String, Object> localExtras, Map<String, String> serverExtras) {
        super.loadInterstitial(context, customEventInterstitialListener, localExtras, serverExtras);
        
        mInterstitialListener = customEventInterstitialListener;
        
        // Get parameters
        String appID;
        if (serverExtras.containsKey("appID")) {
            appID = serverExtras.get("appID");
        } else {
            mInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        
        // Initialise SDK
        mAppnext = new Appnext(context);
        mAppnext.setAdLoadInterface(this);
        mAppnext.setNoAdsInterface(this);
        mAppnext.setPopupOpenedInterface(this);
        mAppnext.setPopupClickedCallback(this);
        mAppnext.setPopupClosedCallback(this);
        mAppnext.setAppID(appID);
    }

    @Override
    protected void onInvalidate() {
        super.onInvalidate();
        
        mInterstitialListener = null;
        mAppnext = null;
    }

    @Override
    protected void showInterstitial() {
        super.showInterstitial();
        
        mAppnext.showBubble();
    }

    /** AppNext Listener Methods **/

    @Override
    public void adLoaded() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void popupClosed() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }

    @Override
    public void popupClicked() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialClicked();
    }

    @Override
    public void popupOpened() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialShown();
    }

    @Override
    public void noAds() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
    }
    

}
