/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.mopub.mobileads.CustomEventInterstitial;

/**
 * Template and common functions for implementation of MoPub CustomEventInterstitials
 */
public class CustomEventTemplate extends CustomEventInterstitial {
    private static final String TAG = "CustomEvent";
    private static final boolean DEBUG = true;
    
    private String getClassName() {
        return this.getClass().getName();
    }
    
    /**
     * Helper function to log information about CustomEventInterstitials.
     * Prints a message to the debug log, including the current class name.
     */
    protected void log(String message) {
        if(DEBUG) { Log.d(TAG, getClassName() + ": " + message); }
    }
    
    /**
     * Child classes should call super.loadInterstitial(...) to preserve logging.
     */
    @Override
    protected void loadInterstitial(Context arg0,
            CustomEventInterstitialListener arg1, Map<String, Object> arg2,
            Map<String, String> arg3) {
        log("loadInterstitial()");
    }

    /**
     * Child classes should call super.onInvalidate() to preserve logging.
     */
    @Override
    protected void onInvalidate() {
        log("onInvalidate()");
    }

    /**
     * Child classes should call super.showInterstitial() to preserve logging.
     */
    @Override
    protected void showInterstitial() {
        log("showInterstitial()");
    }

}
