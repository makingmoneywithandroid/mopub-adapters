/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.inneractive.api.ads.InneractiveAd;
import com.inneractive.api.ads.InneractiveAdListener;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.MoPubErrorCode;

public class InneractiveInterstitial extends CustomEventTemplate implements AdEventListener, AdDisplayListener, InneractiveAdListener {
    private CustomEventInterstitialListener mInterstitialListener;
    private Context mContext;
    private String appID = "";

    @Override
    protected void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener,
            Map<String, Object> localExtras, Map<String, String> serverExtras) {
        super.loadInterstitial(context, customEventInterstitialListener, localExtras, serverExtras);
        
        mInterstitialListener = customEventInterstitialListener;
        
        // Get parameters
        if (serverExtras.containsKey("appID")) {
            appID = serverExtras.get("appID");
        } else {
            mInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        
        // Initialise SDK
        mContext = context;
        
        // Pretend the network is loaded (since there's no preload method)
        mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    protected void onInvalidate() {
        super.onInvalidate();
        
        mInterstitialListener = null;
        mContext = null;
    }

    @Override
    protected void showInterstitial() {
        super.showInterstitial();
        
        InneractiveAd.displayInterstitialAd(mContext, new LinearLayout(mContext), appID, this);
    }

    /** Inneractive Listener Methods **/

    @Override
    public void onIaAdClicked() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialClicked();
    }

    @Override
    public void onIaAdExpand() {
        
    }

    @Override
    public void onIaAdExpandClosed() {
        
    }

    @Override
    public void onIaAdFailed() {
        log("onIaAdFailed()");
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
    }

    @Override
    public void onIaAdReceived() {
        log("onIaAdReceived()");
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void onIaAdResize() {
        
    }

    @Override
    public void onIaAdResizeClosed() {
        
    }

    @Override
    public void onIaDefaultAdReceived() {
        log("onIaDefaultAdReceived()");
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void onIaDismissScreen() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }

    @Override
    public void adDisplayed(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialShown();
    }

    @Override
    public void adHidden(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }

    @Override
    public void onFailedToReceiveAd(Ad arg0) {
        log("onFailedToReceiveAd()");
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
    }

    @Override
    public void onReceiveAd(Ad arg0) {
        log("onReceiveAd()");
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

}
