/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.ironsource.mobilcore.CallbackResponse;
import com.ironsource.mobilcore.IOnOfferwallReadyListener;
import com.ironsource.mobilcore.MobileCore;
import com.ironsource.mobilcore.MobileCore.LOG_TYPE;
import com.mopub.mobileads.MoPubErrorCode;

public class MobileCoreInterstitial extends CustomEventTemplate implements CallbackResponse, IOnOfferwallReadyListener {
    private CustomEventInterstitialListener mInterstitialListener;
    //NOTE: MobileCore needs an Activity but all we have is a Context.
    //      We're being naughty by casting this variable to an Activity when required.
    //      Not really the best idea....
    private Context mContext;

    @Override
    protected void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener,
            Map<String, Object> localExtras, Map<String, String> serverExtras) {
        super.loadInterstitial(context, customEventInterstitialListener, localExtras, serverExtras);
        
        mInterstitialListener = customEventInterstitialListener;
        
        // Get parameters
        String appID;
        if (serverExtras.containsKey("appID")) {
            appID = serverExtras.get("appID");
        } else {
            mInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        
        // Initialise SDK
        mContext = context;
        MobileCore.init((Activity)mContext, appID, LOG_TYPE.DEBUG);
        MobileCore.addOfferWallReadyListener(this);
    }

    @Override
    protected void onInvalidate() {
        super.onInvalidate();
        
        mInterstitialListener = null;
        mContext = null;
    }

    @Override
    protected void showInterstitial() {
        super.showInterstitial();
        
        MobileCore.showOfferWall((Activity)mContext, this);
        mInterstitialListener.onInterstitialShown();
    }

    /** MobileCore Listener Methods **/

    @Override
    public void onConfirmation(TYPE arg0) {
        //NOTE: The offerwall hasn't necessarily been dismissed here
        //      But for simple reporting to MoPub, we assume it has.
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }

    @Override
    public void onOfferWallReady() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

}
