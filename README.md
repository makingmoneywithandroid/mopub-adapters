Description
===========

A set of adapters to integrate third-party ad networks into the MoPub Android SDK.

Features
========

* Integrates third-party ad networks which aren't officially supported by MoPub.
* Fetches App ID / Ad Unit ID from the MoPub server variables when required.
* Includes (optional) debug log to help verify operation.

How To Use
==========

* Add the third-party ad network SDKs to your project, and add any required manifest entries as described on the vendor's website.
* Copy the relevant `.java` files from this repository into your project.
  * Files should be copied to the directory `<project>/src/com/makingmoneywithandroid/ads/`.
  * You must include `CustomEventTemplate.java`, as all the other classes depend on this one.
* Add new "Custom Native" network to MoPub, using the full class name of the adapters you have copied.
