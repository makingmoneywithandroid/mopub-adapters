/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.MoPubErrorCode;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdDisplayListener;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.StartAppAd;

public class StartAppInterstitial extends CustomEventTemplate implements AdEventListener, AdDisplayListener {
    private CustomEventInterstitialListener mInterstitialListener;
    private StartAppAd startAppAd = null;

    @Override
    protected void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener,
            Map<String, Object> localExtras, Map<String, String> serverExtras) {
        super.loadInterstitial(context, customEventInterstitialListener, localExtras, serverExtras);
        
        mInterstitialListener = customEventInterstitialListener;
        
        // Initialise SDK
        startAppAd = new StartAppAd(context);
        startAppAd.onResume();
        startAppAd.loadAd(this);
    }

    @Override
    protected void onInvalidate() {
        super.onInvalidate();
        
        mInterstitialListener = null;
        startAppAd = null;
    }

    @Override
    protected void showInterstitial() {
        super.showInterstitial();
        
        startAppAd.showAd(this);
    }

    /** StartApp Listener Methods **/
    
    @Override
    public void onFailedToReceiveAd(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
    }

    @Override
    public void onReceiveAd(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void adDisplayed(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialShown();
    }

    @Override
    public void adHidden(Ad arg0) {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }   

}
