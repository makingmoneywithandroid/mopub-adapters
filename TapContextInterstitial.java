/*
 * Copyright (C) 2013 David Webb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.makingmoneywithandroid.ads;

import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.MoPubErrorCode;
import com.tapcontext.AdCallback;
import com.tapcontext.TapContextSDK;

public class TapContextInterstitial extends CustomEventTemplate implements AdCallback {
    private CustomEventInterstitialListener mInterstitialListener;
    private TapContextSDK mSDK;

    @Override
    protected void loadInterstitial(Context context, CustomEventInterstitialListener customEventInterstitialListener,
            Map<String, Object> localExtras, Map<String, String> serverExtras) {
        super.loadInterstitial(context, customEventInterstitialListener, localExtras, serverExtras);
        
        mInterstitialListener = customEventInterstitialListener;
        
        // Initialise SDK
        mSDK = new TapContextSDK(context.getApplicationContext());
        mSDK.initialize();
        
        // Tell the listener we're ready
        mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    protected void onInvalidate() {
        super.onInvalidate();
        
        mInterstitialListener = null;
        mSDK = null;
    }

    @Override
    protected void showInterstitial() {
        super.showInterstitial();
        
        mSDK.showAd(this);
    }
    
    /** TapContext Listener Methods **/

    @Override
    public void onAdNotShown() {
        // It's too late to tell mInterstitialListener, but we try anyway
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
    }

    @Override
    public void onAdClicked() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialClicked();
    }

    @Override
    public void onAdClose() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialDismissed();
    }

    @Override
    public void onAdShown() {
        if(mInterstitialListener!=null) mInterstitialListener.onInterstitialShown();
    }

}
